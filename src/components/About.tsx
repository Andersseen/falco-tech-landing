import { useRef } from "react";
import { motion, useScroll, useTransform } from "framer-motion";
import image1 from "../assets/4.webp";
import image2 from "../assets/5.webp";
import image3 from "../assets/6.webp";
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/ui/accordion";

export default function About() {
  return (
    <div id="features">
      <TextParallaxContent
        imgUrl={image3}
        subheading="Servicio"
        heading="Control fauna"
      >
        <FaunaContent />
      </TextParallaxContent>
      <TextParallaxContent
        imgUrl={image2}
        subheading="Servicio"
        heading="Consultora ambiental"
      >
        <EnvironmentContent />
      </TextParallaxContent>
      <TextParallaxContent
        imgUrl={image1}
        subheading="Servicio"
        heading="Drones"
      >
        <DronContent />
      </TextParallaxContent>
    </div>
  );
}

const IMG_PADDING = 12;

const TextParallaxContent = ({ imgUrl, subheading, heading, children }) => {
  return (
    <div
      style={{
        paddingLeft: IMG_PADDING,
        paddingRight: IMG_PADDING,
      }}
    >
      <div className="relative h-[150vh]">
        <StickyImage imgUrl={imgUrl} />
        <OverlayCopy heading={heading} subheading={subheading} />
      </div>
      {children}
    </div>
  );
};

const StickyImage = ({ imgUrl }) => {
  const targetRef = useRef(null);
  const { scrollYProgress } = useScroll({
    target: targetRef,
    offset: ["end end", "end start"],
  });

  const scale = useTransform(scrollYProgress, [0, 1], [1, 0.85]);
  const opacity = useTransform(scrollYProgress, [0, 1], [1, 0]);

  return (
    <motion.div
      style={{
        backgroundImage: `url(${imgUrl})`,
        backgroundSize: "cover",
        backgroundPosition: "center",
        height: `calc(100vh - ${IMG_PADDING * 2}px)`,
        top: IMG_PADDING,
        scale,
      }}
      ref={targetRef}
      className="sticky z-0 overflow-hidden rounded-3xl"
    >
      <motion.div
        className="absolute inset-0 bg-neutral-950/70"
        style={{
          opacity,
        }}
      />
    </motion.div>
  );
};

const OverlayCopy = ({ subheading, heading }) => {
  const targetRef = useRef(null);
  const { scrollYProgress } = useScroll({
    target: targetRef,
    offset: ["start end", "end start"],
  });

  const y = useTransform(scrollYProgress, [0, 1], [250, -250]);
  const opacity = useTransform(scrollYProgress, [0.25, 0.5, 0.75], [0, 1, 0]);

  return (
    <motion.div
      style={{
        y,
        opacity,
      }}
      ref={targetRef}
      className="absolute left-0 top-0 flex h-screen w-full flex-col items-center justify-center text-white"
    >
      <p className="mb-2 text-center text-xl md:mb-4 md:text-3xl">
        {subheading}
      </p>
      <p className="text-center text-4xl font-bold md:text-7xl">{heading}</p>
    </motion.div>
  );
};

const FaunaContent = () => (
  <div className="mx-auto flex flex-col justify-center px-8 md:px-40 mb-24">
    <p className="mb-4 text-xl text-[--foreground-color] md:text-2xl">
      En FALCOTECH, contamos con un equipo altamente profesional compuesto por
      técnicos y expertos en control y manejo de fauna, incluyendo especialistas
      en cetrería. Nuestro equipo combina conocimientos profundos en técnicas
      tradicionales y modernas, lo que nos permite abordar diversas situaciones
      con una perspectiva integral y adaptada a cada caso.
    </p>
    <p className="mb-4 text-xl text-[--foreground-color] md:text-2xl">
      Nuestro compromiso es ofrecer servicios de la más alta calidad,
      garantizando resultados óptimos y sostenibles para la convivencia entre la
      actividad humana y la conservación de la biodiversidad.
    </p>
    <p className="mb-8 text-xl text-[--foreground-color] md:text-2xl">
      Para desarrollar nuestro servicio de control de fauna, utilizamos
      cetrería, aprovechando la habilidad de aves rapaces entrenadas para
      gestionar poblaciones de fauna de manera natural. Complementamos esta
      técnica con el uso de drones adaptados, que nos permiten monitorizar y
      controlar áreas extensas con precisión y eficacia. Además, implementamos
      sistemas de captura diseñados para ser respetuosos con las especies,
      asegurando su bienestar y minimizando el impacto en su entorno natural.
    </p>
    <AccordionFuana />
  </div>
);

export function AccordionFuana() {
  return (
    <Accordion type="multiple" className="w-full">
      <AccordionItem value="item-1">
        <AccordionTrigger>Aeropuertos:</AccordionTrigger>
        <AccordionContent>
          Gestionamos la fauna para minimizar los riesgos de colisiones con
          aeronaves, garantizando la seguridad en las operaciones
          aeroportuarias.
        </AccordionContent>
      </AccordionItem>
      <AccordionItem value="item-2">
        <AccordionTrigger>Zonas Urbanas:</AccordionTrigger>
        <AccordionContent>
          Desarrollamos soluciones que facilitan la convivencia entre la fauna y
          la población en áreas urbanas, manteniendo el bienestar de ambos.
        </AccordionContent>
      </AccordionItem>
      <AccordionItem value="item-3">
        <AccordionTrigger>Cultivo:</AccordionTrigger>
        <AccordionContent>
          Implementamos estrategias de control de fauna que preservan la
          integridad y productividad de las áreas agrícolas, evitando daños a
          los cultivos.
        </AccordionContent>
      </AccordionItem>
      <AccordionItem value="item-4">
        <AccordionTrigger>Vertederos:</AccordionTrigger>
        <AccordionContent>
          Controlamos la fauna en vertederos para evitar problemas de salud
          pública y daños ambientales, asegurando una gestión eficiente de los
          residuos.
        </AccordionContent>
      </AccordionItem>
      <AccordionItem value="item-5">
        <AccordionTrigger>Acuicultura:</AccordionTrigger>
        <AccordionContent>
          Protegemos las instalaciones acuícolas de depredadores y otras
          amenazas, asegurando un entorno seguro y productivo.
        </AccordionContent>
      </AccordionItem>
    </Accordion>
  );
}

const EnvironmentContent = () => (
  <div className="mx-auto flex flex-col justify-center px-8 md:px-40 mb-24">
    <p className="mb-4 text-xl text-[--foreground-color] md:text-2xl">
      Nuestro equipo está compuesto por técnicos y biólogos con amplia
      experiencia en el seguimiento y estudio de fauna, estudios de
      Biodiversidad, Evaluación de Impacto Ambiental y análisis estadístico y
      geoespacial lo que les permite abordar diversas situaciones con solvencia
      y un enfoque especializado.
    </p>
    <p className="mb-4 text-xl text-[--foreground-color] md:text-2xl">
      Los esfuerzos diarios para prestar un servicio de alto nivel, hace que
      nuestro equipo técnico domine los programas informáticos más avanzados
      para la planificación y diseño de las metodologías de estudio y el
      procesamiento de datos que permita sacar el máximo partido a los trabajos
      realizados en campo como prospecciones botánicas, estudios de avifauna,
      quirópteros y herpetofauna, entre otros.
    </p>
  </div>
);
const DronContent = () => (
  <div className="mx-auto flex flex-col justify-center px-8 md:px-40 mb-24">
    <p className="mb-4 text-xl text-[--foreground-color] md:text-2xl">
      Nuestra empresa integra tecnología UAV (Unmanned Aerial Vehicle) para
      optimizar nuestros servicios, proporcionando soluciones avanzadas y
      precisas. Ofrecemos una gama de servicios con drones, incluyendo
      grabaciones de alta calidad y detalladas inspecciones de infraestructuras
      y terrenos. Además, desarrollamos drones especializados, personalizados
      para realizar trabajos específicos según las necesidades del proyecto.
    </p>
  </div>
);
