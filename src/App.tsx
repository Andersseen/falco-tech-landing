import { ScrollToTop } from "@/components/ScrollToTop";
import "./App.css";
import Hero from "@/components/Hero";
import Features from "@/components/Features";
import Footer from "@/components/Footer";
import Navbar from "@/components/Navbar";
// import About from "@/components/About";
import GlobeWithForm from "./components/GlobeWithForm";
import About2 from "./components/About2";
import Carousel from "./components/Carousel";

function App() {
  return (
    <>
      <Navbar />
      <Hero />
      <Features />
      <About2 />
      <Carousel />
      <GlobeWithForm />
      <Footer />
      <ScrollToTop />
    </>
  );
}

export default App;
